(function($) {
    // "use strict";
    // index
    $('.banner-slider').slick({
        autoplay: true,
        autoplaySpeed: 2000,
        dots: true,
        arrows: false,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear'
    });
    // pro-de
    $('.slider-for').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        arrows: false,
        fade: true
        // asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
        dots: true,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: true,
        asNavFor: '.slider-for',
        focusOnSelect: true
    });
})(jQuery);