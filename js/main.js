(function($) {
    "use strict";

    /*------------------------
        WOW active 
    ------------------------- */
    new WOW().init();
    /*------------------------
        dec_height
    ------------------------- */
    var dec_height = function(first) {
        var content = $('body').height(),
            win_h = $(window).height(),
            main_h = $('#main').outerHeight(),
            need_h = win_h - content;
        if (content < win_h) {
            $('#main').css("min-height", main_h + need_h);
        }
    };
    $(window).on('load', function() {
        var winW = $(window).width();
        dec_height();
    });
    $(window).resize(function() {
        var winW = $(window).width();
        dec_height();
    });
    /*------------------------
        jQuery MeanMenu
    ------------------------- */
    $('#mobile-menu-active').meanmenu({
        meanScreenWidth: "991",
        meanMenuContainer: ".mobile-menu-area .mobile-menu",
    });
    /*------------------------------
       fixed stickymenu  
    --------------------------------*/
    var stickymenu = document.getElementById("stickymenu")
    var stickymenuoffset = stickymenu.offsetTop
    var scrolltimer

    window.addEventListener("scroll", function(e) {
        requestAnimationFrame(function() {
            if (window.pageYOffset > stickymenuoffset) {
                stickymenu.classList.add('sticky')
            } else {
                stickymenu.classList.remove('sticky')
            }
        })
    });
    /*------------------------------
       goto top & inquiry  
    --------------------------------*/
    $(window).scroll(function() {
        var $win = $(window);
        if ($win.scrollTop() > 200) {
            $('#bk-top').addClass('active');
        } else {
            $('#bk-top').removeClass('active');
        }
    });

    $('.js-gotop').on('click', function(event) {

        event.preventDefault();

        $('html, body').animate({
            scrollTop: $('html').offset().top
        }, 500, 'easeInOutExpo');

        return false;
    });
    
})(jQuery);