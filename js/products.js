$(function() {
    var dec_height = function(first) {
        var content = $('body').height(),
            win_h = $(window).height(),
            main_h = $('#main').outerHeight(),
            need_h = win_h - content;
        if (content < win_h || typeof first != 'undefined') {
            $('#main').css("min-height", main_h + need_h);
        }
    };
    dec_height(true);
    $('#pro-series .list-group-item').on('click', function() {
        dec_height();
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $('#pro-series .list-group-item.active').removeClass('active');
            $(this).addClass('active');
        }
    });
    $('.gallery').each(function() {
        $(this).magnificPopup({
            delegate: 'a.image-popup',
            type: 'image',
            image: {
                cursor: 'mfp-zoom-out-cur',
                titleSrc: 'title',
                verticalFit: true,
                tError: '<a href="%url%">The image</a> could not be loaded.'
            },
            gallery: {
                enabled: true
            }
        });
    });
    $("body").swipe({
        swipeLeft: function(event, direction, distance, duration, fingerCount) {
            $(".mfp-arrow-left").magnificPopup("prev");
        },
        swipeRight: function() {
            $(".mfp-arrow-right").magnificPopup("next");
        },
        threshold: 50
    });
    $('.image-popup').magnificPopup({
        type: 'image',
        removalDelay: 300,
        titleSrc: 'title',
        gallery: {
            enabled: true
        }
    });
});